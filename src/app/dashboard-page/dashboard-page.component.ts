import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseListObservable } from 'angularfire2';

import { UserService } from '../user.service';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {

  initialized = false;
  public activeRoom;
  public rooms: FirebaseListObservable<any>;
  public roomList = {
    owned: [],
    invited: []
  };
  public newRoomName: string;
  constructor(public roomService: RoomService, public userService: UserService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    let uid = this.route.snapshot.queryParams['uid'];
    this.userService.setUid(uid);
    this.userService.storeUserInfo().then(() => {
      this.initialized = true;
      this.roomService.rooms
        .subscribe(data => {
          this.sortAndFilterRooms(data)
        });
    });
  }

  sortAndFilterRooms(roomData) {
    this.roomList.owned = [];
    this.roomList.invited = [];
    var userRooms = roomData.filter(room => {
      return room.members.indexOf(this.userService.userName) !== -1;
    });
    userRooms.map(room => {
      if (room.leader === this.userService.userName) {
        this.roomList.owned.push(room);
      } else {
        this.roomList.invited.push(room);
      }
    });
  }

  changeRoom(room) {
    this.activeRoom = room;
  }

  addRoom() {
    var roomName = this.newRoomName;
    if (roomName) {
      this.roomService.addRoom(roomName, this.userService.userName, [this.userService.userName]);
      this.newRoomName = '';
    }
  }

  removeRoom(key) {
    this.roomService.removeRoom(key);
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['']);
  }
}
