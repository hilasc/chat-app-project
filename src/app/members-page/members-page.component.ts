import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-members-page',
  templateUrl: './members-page.component.html',
  styleUrls: ['./members-page.component.css']
})
export class MembersPageComponent implements OnInit {

  public error: any;
  public modal: string;

  constructor(private userService: UserService, private router: Router) {
    this.modal = 'login';
  }

  ngOnInit() {
  }

  /**
   * Function to show modal
   * @param value ['login', 'register'].. set which modal to show
   */
  showModal(value): void {
    this.modal = value;
  }

  /**
   * Event on User registeration submission
   * @param event register form submit evt
   * @param name username
   * @param password password
   * @param photo photo url
   */
  registerUser(event, name, password, photo) {
    event.preventDefault();
    this.userService.registerUser(name, password, photo).then(uid => {
      this.router.navigate(['dashboard'], { queryParams: { uid: uid } });
    })
      .catch((error) => {
        this.error = error;
        console.log(this.error);
      });
  }

  /**
   * Event on User Login submission
   * @param event event login form submit evt
   * @param name username
   * @param password password
   */
  loginUser(event, name, password) {
    event.preventDefault();
    this.userService.loginUser(name, password).then(uid => {
      this.router.navigate(['dashboard'], { queryParams: { uid: uid } });
    })
      .catch((error: any) => {
        if (error) {
          this.error = "Username or Passwrod incorrect! Please try again";
          console.log(this.error);
        }
      });
  }
}
