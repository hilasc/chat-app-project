import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public userService: UserService, private router: Router) {
    this.userService.checkUser()
      .then(uid => {
        this.router.navigate(['dashboard'], { queryParams: { uid: uid } });
      }, failure => {
        this.router.navigate(['']);
      })
  }
}
