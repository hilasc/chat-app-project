// angular core imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

// angularfire library for firebase communication
import { AngularFireModule } from 'angularfire2';


import { AppComponent } from './app.component';
import { MembersPageComponent } from './members-page/members-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { RoomComponent } from './room/room.component';

import { UserService } from './user.service';
import { RoomService} from './room.service';

import { NguiAutoCompleteModule } from '@ngui/auto-complete';

export const config = {
    apiKey: "AIzaSyBerlLPCgXz52xhuq816laL2I8IssduNcw",
    authDomain: "chat-28403.firebaseapp.com",
    databaseURL: "https://chat-28403.firebaseio.com",
    projectId: "chat-28403",
    storageBucket: "chat-28403.appspot.com",
    messagingSenderId: "177333067483"
  };

const routes: Routes = [
  { path: '', component: MembersPageComponent },
  { path: 'dashboard', component: DashboardPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MembersPageComponent,
    DashboardPageComponent,
    RoomComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NguiAutoCompleteModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(config)
  ],
  providers: [
    UserService,
    RoomService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
