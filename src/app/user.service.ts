/**
 * User Service
 * Responsible for user related methods like get/set user information, login, registration, auth and such.
 */

import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseObjectObservable } from 'angularfire2';

@Injectable()
export class UserService {

  // firebase auth uid
  private uid: string;
  public userName: string;
  public photo: string;
  public roomList: string[];
  private tempEmail: string;
  public user: FirebaseObjectObservable<any>;

  constructor(public af: AngularFire) {
    this.tempEmail = '@hachidankin.com';
  }

  /**
   * Check firebase auth.
   */
  checkUser() {
    return new Promise((resolve, reject) => {
      this.af.auth.subscribe(
        (auth) => {
          // failed login
          if (auth == null) {
            reject();
          } else {
            resolve(auth.uid);
          }
        }
      )
    });
  }

  // setters and getters for uid
  setUid(uid) {
    this.uid = uid;
  }
  getUid() {
    return this.uid;
  }


  /**
   * Function to register user via firebase auth
   * @param userName {string} username
   * @param password {string} password of user
   * * @param photo {string} photo url of user
   */
  registerUser(userName, password, photo) {
    return new Promise((resolve, reject) => {
      let tempEmail = userName + this.tempEmail
      this.createUser(tempEmail, password)
        .then(userDetail => {
          this.saveUserDetails(userDetail.uid, userName, photo)
            .then(success => {
              resolve(userDetail.uid);
            });
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  // Create User via Firebase auth
  createUser(email, password) {
    return this.af.auth.createUser({
      email: email,
      password: password
    });
  }
  // Save User Other Info
  saveUserDetails(uid, userName, photo) {
    return this.af.database.object('users/' + uid).set({
      name: userName,
      photo: photo,
      roomList: []
    });
  }


  /**
   * Function to login User. Check firebase auth.
   * @param userName username
   * @param password password
   */
  loginUser(userName, password) {
    return new Promise((resolve, reject) => {
      let tempEmail = userName + this.tempEmail;
      this.checkFirebaseAuth(tempEmail, password)
        .then(userAuthDetail => {
          resolve(userAuthDetail.uid);
        })
        .catch(error => {
          reject(error);
        })
    })
  }
  // firebase email password check
  checkFirebaseAuth(email, password) {
    return this.af.auth.login({
      email: email, password: password
    }, {
        provider: AuthProviders.Password, method: AuthMethods.Password,
      });
  }


  /**
   * Fcuntion to store user info locally.
   */
  storeUserInfo() {
    return new Promise(resolve => {
      this.getUserData().subscribe(snapshot => {
        let userInfo = snapshot.val();
        this.userName = userInfo.name;
        this.photo = userInfo.photo;
        this.roomList = userInfo.roomList;
        resolve();
      });
    });
  }
  /**
   * method to get user info from firebase
   */
  getUserData() {
    return this.af.database.object('users/' + this.uid, { preserveSnapshot: true });
  }

  logout() {
    return this.af.auth.logout();
  }
}
