import { Component, Input, OnInit, OnChanges, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { UserService } from '../user.service';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit, OnChanges, AfterViewChecked {
  @Input() room;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  messages: FirebaseListObservable<any[]>;
  public newMessage: string;
  public userToInvite: '';
  public userLeft: string[];
  constructor(public af: AngularFire, public userService: UserService, public roomService: RoomService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    this.room = changes.room.currentValue;
    this.showMsg();
    this.usersToInvite();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  showMsg() {
    this.messages = this.af.database.list('messages', {
      query: {
        orderByChild: 'room',
        equalTo: this.room.name
      }
    });
  }

  usersToInvite() {
    const query$ = this.userService.af.database.list('users', {
      preserveSnapshot: true
    });
    query$.subscribe(users => {
      this.userLeft = [];
      users.forEach(user => {
        let userVal = user.val();
        if (this.room.members.indexOf(userVal.name) === -1) {
          this.userLeft.push(userVal.name);
        }
      });
    });
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }
    catch (err) { }
  } 

  sendMessage() {
    this.af.database.list('messages').push(
      {
        text: this.newMessage,
        room: this.room.name,
        name: this.userService.userName,
        time: Date.now(),
        photo: this.userService.photo
      }
    ).then(success => {
      this.newMessage = '';
    });
  }

  inviteUser() {
    let userList = [];
    userList.push.apply(userList, this.room.members);
    userList.push(this.userToInvite);
    this.room.members = userList;
    this.roomService.inviteUser(this.room.$key, userList);
    let index = this.userLeft.indexOf(this.userToInvite);
    if (index > -1) {
      this.userLeft.splice(index, 1);
    }
    this.userToInvite = '';
  }

  removeUser(userName) {
    let userList = this.room.members;
    var index = userList.indexOf(userName);
    if (index > -1) {
      userList.splice(index, 1);
    }
    this.room.members = userList;
    this.roomService.inviteUser(this.room.$key, userList);
    this.userLeft.push(userName);
  }

  isYou(name) {
    if (name == this.userService.userName) {
      return true;
    } else {
      return false;
    }
  }

  isMe(name) {
    if (name == this.userService.userName) {
      return false;
    } else {
      return true;
    }
  }

}
