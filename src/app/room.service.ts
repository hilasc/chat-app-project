import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';


@Injectable()
export class RoomService {

  public rooms: FirebaseListObservable<any>;
  constructor(public af: AngularFire) {
    this.rooms = this.af.database.list('rooms');
  }

  addRoom(roomName, leader, members) {
    this.rooms.push({
      name: roomName,
      leader: leader,
      members: members
    });
  }

  removeRoom(key) {
    this.rooms.remove(key);
  }

  inviteUser(key, userList) {
    this.rooms.update(key, { members: userList });
  }


}
