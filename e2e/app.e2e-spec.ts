import { ChatWorkPage } from './app.po';

describe('chat-work App', () => {
  let page: ChatWorkPage;

  beforeEach(() => {
    page = new ChatWorkPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
